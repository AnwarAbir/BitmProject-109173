<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 3/4/2016
 * Time: 2:31 PM
 */
class calculator
{
    private $num1, $num2;

    function __construct($num1, $num2)
    {
        $this->num1 = $num1;
        $this->num2 = $num2;
    }

    function test()
    {
        echo "$this->num1 & $this->num2<br/>";
    }

    function add()
    {
        $result = $this->num1 + $this->num2;
        echo "The addition of $this->num1 & $this->num2 is: $result";
    }

    function sub()
    {
        $result = $this->num1 - $this->num2;
        echo "The Substraction of $this->num1 & $this->num2 is: $result";
    }

    function mul()
    {
        $result = $this->num1 * $this->num2;
        echo "The multiplication of $this->num1 & $this->num2 is: $result";
    }

    function div()
    {
        if ($this->num2 == 0) {
            echo "Infinity";
        } else {
            $result = $this->num1 / $this->num2;
            echo "The division of $this->num1 & $this->num2 is: $result";
        }
    }
}