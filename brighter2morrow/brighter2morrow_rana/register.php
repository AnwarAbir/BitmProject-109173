<head>
    <link href="css/login.css" rel="stylesheet" type="text/css"/>
    <script src="js/login.js" type="text/javascript"></script>
</head>

<body>
    <div class="container">

        <form id="signup">

            <div class="header">

                <h3 style="text-align: center">Sign Up</h3>

                <p style="text-align: center">You want to fill out this form</p>

            </div>

            <div class="sep"></div>

            <div class="inputs">

                <input type="email" placeholder="e-mail" autofocus />

                <input type="password" placeholder="Password" />
                <input type="password" placeholder="Repeat Password" />

                <div class="checkboxy">
                    <input name="cecky" id="checky" value="1" type="checkbox" /><label class="terms">I accept the terms of use</label>
                </div>

                <a id="submit" href="#">SIGN UP</a>
                <div class="checkboxy">
                    <a style="color: #2aabd2" href="login.php">Already Registered?</a>
                </div>

            </div>

        </form>

    </div>
    ​
</body>