<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\gender\gender;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';

        $delete = new gender();
        $delete->setId($_GET['id']);
        $delete->delete();

//        $debug = new utility();
//        $debug->debug($_GET);
        ?>
    </body>
</html>
