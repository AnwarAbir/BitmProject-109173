<?php
include_once '../../../../vendor/autoload.php';
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        use App\Bitm\SEID_109173\Book\book;
        use App\Bitm\SEID_109173\utility\utility;
        
//        $debug = new utility();
//        $debug->debug($_GET);
        
        $restoreBook = new book();
        $restoreBook->setId($_GET['id']);
        $restoreBook->restore();
        header('location:index.php');
        ?>
    </body>
</html>
