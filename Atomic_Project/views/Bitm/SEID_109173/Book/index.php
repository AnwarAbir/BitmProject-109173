<?php
include_once '../../../../vendor/autoload.php';
if (!isset($_SESSION['msg'])) {
  session_start();
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Book</title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';

        echo "<h3 style = 'text-allign:center'>A good Book is like a 100 friends and a good friend is like a library</h3><Br/><Br/>";

        use App\Bitm\SEID_109173\Book\book;

$listViewObj = new book();
        $allBooks = $listViewObj->listView();
//        echo "<pre>";
//        print_r($allData);
//        echo "<pre>";
        $counter = 0;
        ?>
        <!--<a href="create.php">Create Page</a>-->

        <table border="1">
            <tr>
                <th>SI</th>
                <th>Title</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            <?php foreach ($allBooks as $oneBook) { ?>
                <tr>
                    <td><?php echo ++$counter; ?></td>
                    <td><?php echo $oneBook['Book_title'] ?></td>
                    <td><?php echo $oneBook['created'] ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $oneBook['id'] ?>">View Details</a> |
                        <a href="edit.php?id=<?php echo $oneBook['id'] ?>">Update</a> |
                        <a href="trash.php?id=<?php echo $oneBook['id'] ?>">Delete</a>
                    </td>
                </tr>
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
//                echo $_SESSION['msg'];
                echo '<script type="text/javascript">alert("' . $_SESSION['msg'] . '"); </script>';
                session_unset("msg");
            }
            ?>
        </table>
    </body>
</html>
