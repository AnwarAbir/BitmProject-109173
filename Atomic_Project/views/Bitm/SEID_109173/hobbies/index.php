<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\hobbies\hobbies;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL ^ E_DEPRECATED);
        include_once '../menu/menu.php';


        $showAllHobbies = new hobbies();
        $allHobbies = $showAllHobbies->index();


        $debug = new utility();
//        $debug->debug($allHobbies);
        ?>

        <table border="1">
            <tr>
                <th>Id</th>
                <th>Hobby</th>
                <th>Action</th>
            </tr>
            <?php foreach ($allHobbies as $oneHobby) { ?>
                <tr>
                    <td><?php echo $oneHobby['id']; ?></td>
                    <td><?php echo $oneHobby['hobby']; ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $oneHobby['id']; ?>">Show Details</a> |
                        <a href="edit.php?id=<?php echo $oneHobby['id']; ?>">Update</a> |
                        <a href="delete.php?id=<?php echo $oneHobby['id']; ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>

        </table>
    </body>
</html>
