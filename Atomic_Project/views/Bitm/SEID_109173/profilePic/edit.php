<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\profilePic\profilePic;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
         error_reporting(E_ALL ^ E_DEPRECATED);
        include_once '../menu/menu.php';


        $updateProfile = new profilePic();
        $updateProfile->setId($_GET['id']);
        $updateOneProfile = $updateProfile->show();


        $debug = new utility();
//        $debug->debug($updateOneProfile);
//        die();
        ?>
        <fieldset>
            <legend>Profile Picture upload</legend>
            <form action="update.php" method="post" enctype="multipart/form-data">
                <label for="uname">UserName</label><br/>
                <input type="text" name="name" id="uname" value="<?php echo $updateOneProfile['username'];?>"/><br/>
                <label for="pimage">Upload Profile Pic</label><br/>
                <input type="file" name="image" id="pimage"/><img src="../../../../image/<?php echo $updateOneProfile['imagename']; ?>" height="200" width="200"><br/>
                <input type="hidden" name="id" value="<?php echo $updateOneProfile['id'];?>"/>
                <input type="submit" value="Update"/>
            </form>
        </fieldset>
    </body>
</html>
