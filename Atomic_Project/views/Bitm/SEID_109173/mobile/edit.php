<?php include_once '../../../../vendor/autoload.php'; ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';

        use App\Bitm\SEID_109173\utility\utility;
        use App\Bitm\SEID_109173\mobile\mobile;

        $mobileObj = new mobile();
        $mobileObj->setId($_GET['id']);
        $oldMobile = $mobileObj->show();
        
        $debug = new utility();
//        $debug->debug($oldMobile);
        ?>
        
        
        <fieldset>
            <legend>Update New Mobile</legend>
            <form action="update.php" method="get">
                <label for="MobileTitle">Mobile Name</label><br/>
                <input type="text" name="title" value ="<?php echo $oldMobile['mobile_title'];?>" id="MobileTitle"/><br/>
                <input type="hidden" name="id" value="<?php echo $oldMobile['id'];?>"/>
                <input type="submit" name="submit" value="Update"/>
            </form>
        </fieldset>
    </body>
</html>
