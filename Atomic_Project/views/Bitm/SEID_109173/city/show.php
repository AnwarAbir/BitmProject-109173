<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\gender\gender;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';
        $show = new gender();
        $show->setId($_GET['id']);
        $showAllData = $show->show();

//        $debug = new utility();
//        $debug->debug($showAllData);
        ?>

        <table border="1">
            <tr>
                <th>Serial</th>
                <th>Name</th>
                <th>Email</th>
                <th>Gender</th>
                <th>City</th>
                <th>Created date</th>
                <th>Action</th>
            </tr>
            <tr>
                <td><?php echo $showAllData['id']; ?></td>
                <td><?php echo $showAllData['name']; ?></td>
                <td><?php echo $showAllData['email']; ?></td>
                <td><?php echo $showAllData['gender']; ?></td>
                <td><?php echo $showAllData['city']; ?></td>
                <td><?php echo $showAllData['created']; ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $showAllData['id']; ?>">Update</a> |
                    <a href="trash.php?id=<?php echo $showAllData['id']; ?>">Delete</a>
                </td>
            </tr>
        </table>
    </body>
</html>
