<?php include_once '../../../../vendor/autoload.php'; ?>
<?php
session_start();

if (isset($_SESSION) && !empty($_SESSION)) {
    echo '<script type="text/javascript">alert("' . $_SESSION['msg'] . '"); </script>';
    session_unset();
    session_destroy();
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php include_once '../menu/menu.php'; ?>

        <fieldset>
            <legend>Add a Birthday</legend>
            <form action="store.php" method="get">
                <label>Enter Your Birthday</label><Br/>
                <input type="date" name="bdate"/><br/>
                <input type="submit" value="Save"/>
            </form>
        </fieldset>
    </body>
</html>
