<?php include_once '../../../../vendor/autoload.php'; ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL ^ E_DEPRECATED);
        include_once '../menu/menu.php';

        use App\Bitm\SEID_109173\utility\utility;
        use App\Bitm\SEID_109173\birthday\birthday;

$viewBdate = new birthday();
        $viewBdate->setValues($_GET);
        $viewOneBirthday = $viewBdate->show();


        $debug = new utility();
//        $debug->debug($viewOneBirthday);
        ?>
        <table border="1">
            <tr>
                <th>SI</th>
                <th>Birthday</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            <tr>
                <td><?php echo $viewOneBirthday['id']; ?></td>
                <td><?php echo $viewOneBirthday['bday'] ?></td>
                <td><?php echo $viewOneBirthday['created'] ?></td>
                <td>
                    <a href="edit.php?id=<?php echo $viewOneBirthday['id'] ?>">Update</a> |
                    <a href="trash.php?id=<?php echo $viewOneBirthday['id'] ?>">Delete</a>
                </td>
            </tr>      
        </table>
    </body>
</html>
