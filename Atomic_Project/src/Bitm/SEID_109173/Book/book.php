<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of book
 *
 * @author Web App Develop PHP
 */

namespace App\Bitm\SEID_109173\Book;

class book {

    public $id = "";
    public $book_title = "";

    function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('can nit connect to DB');
        mysql_select_db('109173_project', $conn) or die('can not connect to db');
    }

    function setBook_title($book_title) {
        $this->book_title = $book_title;
    }

    function setId($id) {
        $this->id = $id;
    }

    public function getTitle() {
        return $this->book_title;
    }

    public function store() {
        $query = "INSERT INTO `109173_Project`.`book` (`id`, `Book_title`, `created`) VALUES (NULL, '" . $this->book_title . "', '" . date("Y-m-d H:i:s") . "')";
        session_start();
        if (empty($this->book_title)) {
            $_SESSION["msg"] = "Please Enter Some data";
            header('Location:create.php');
        } else {
            if (mysql_query($query)) {
                $_SESSION["msg"] = "Succesful Added";
//                echo '<script language="javascript">';
//                echo 'alery("Successfully saved")';
//                echo '</script>';
            } else {
                $_SESSION["msg"] = "Sorry Something went wrong";
            }
        }

        header('Location:index.php');
    }

    public function listView() {
        $allData = array();
        $query = "SELECT * FROM `book` WHERE  deleted_at = 0 ORDER BY `book`.`Book_title` ASC";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $allData [] = $row;
        }
        return $allData;
    }

    public function bookShow($id = '') {
        $this->id = $id;
        $query = "SELECT * FROM `book` WHERE `id` = " . $this->id;
        $result = mysql_query($query);
        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function deleteBook($id = '') {
        $this->id = $id;
        $query = "DELETE FROM `book` WHERE `id` =" . $this->id;
        return mysql_query($query);
    }

    public function update() {
        $query = "UPDATE `109173_project`.`book` SET `Book_title` = '" . $this->book_title . "', `updated`='" . date("Y-m-d H:i:s") . "' WHERE `book`.`id` =" . $this->id;
        if (empty($this->book_title)) {
            
        } else {
            if (mysql_query($query)) {
                session_start();
                $_SESSION['msg'] = "Succesfully Updated";
                header('location:index.php');
            } else {
                header("location:edit.php");
            }
        }
    }

    public function trash() {
        session_start();
        $query = "UPDATE `109173_project`.`book` SET  `deleted_at`='" . date("Y-m-d H:i:s") . "' WHERE `book`.`id` =" . $this->id;
        if (mysql_query($query)) {
            $_SESSION['msg'] = "Succesfully Deleted";
            header('location:index.php');
        } else {
            $_SESSION['msg'] = "Something Went Wrong";
        }
    }

    public function trashedItem() {
        $allData = array();
        $query = "SELECT * FROM `book` WHERE  deleted_at != 0";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $allData [] = $row;
        }
        return $allData;
    }

    public function restore() {
        $query = "UPDATE `109173_project`.`book` SET  `deleted_at`='NULL' WHERE `book`.`id` =" . $this->id;
        return mysql_query($query);
    }
//UPDATE `109173_project`.`book` SET `deleted_at` = NULL WHERE `book`.`id` = 27;
}
