<?php

namespace App\Bitm\SEID_109173\birthday;

class birthday {

    private $id = '';
    public $birthday = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Can not connect to server');
        mysql_selectdb("109173_project", $conn) or die("Can not connect to DB");
    }

    function setValues($values = array()) {
        if (array_key_exists('id', $values)) {
            $this->id = $values['id'];
        }
        if (array_key_exists('bdate', $values)) {
            $this->birthday = $values['bdate'];
        }
       
    }

    public function store() {
        $query = "INSERT INTO `birthday` (`id`, `bday`, `created`) VALUES (NULL, '" . $this->birthday . "', '" . date("Y-m-d h:i:s") . "')";
        session_start();
        if (empty($this->birthday)) {
            $_SESSION['msg'] = "Insert valid data";
            header('location:create.php');
        } else {
            if (mysql_query($query)) {
                $_SESSION['msg'] = "Successfully Inserted";
                header('location:index.php');
            } else {
                $_SESSION['msg'] = "Something Went wrong";
                header('location:create.php');
            }
        }
    }

    public function index() {
        $allBirthday = array();
        $qurey = "SELECT * FROM `birthday` WHERE `deleted_at` IS NULL";
        $result = mysql_query($qurey);

        while ($row = mysql_fetch_assoc($result)) {
            $allBirthday [] = $row;
        }
        return $allBirthday;
    }

    public function show() {
        $query = "SELECT * FROM `birthday` WHERE `id` = " . $this->id;
        $result = mysql_query($query);
        $singledata=   mysql_fetch_assoc($result);
        return $singledata;
    }

    public function update() {
//        session_start();
        $query = "UPDATE `109173_project`.`birthday` SET `updated` = '" . $this->birthday . "' WHERE `birthday`.`id` =" . $this->id;
        if (mysql_query($query)) {
            header('location:index.php');
            $_SESSION['msg'] = "Successfully Updated";
        } else {
            $_SESSION['msg'] = "Something Went wrong";
            header('location:edit.php');
        }
    }

}
