-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2016 at 07:26 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `109173_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE `admin_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `login_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`id`, `name`, `email`, `password`, `login_date`) VALUES
(1, 'Md. Anwarul Islam Abir', 'abir@gmail.com', 'as12345 ', '0000-00-00 00:00:00'),
(4, 'Nuru', 'nuru@gmail.com', 'nuru123', '2016-04-04 01:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `bday` date NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `bday`, `created`, `updated`, `deleted_at`) VALUES
(4, '2016-03-15', '2016-03-26 02:24:56', '2016-03-15 00:00:00', NULL),
(5, '2016-03-15', '2016-03-26 02:31:33', '2016-03-15 00:00:00', NULL),
(6, '2016-03-22', '2016-03-26 02:31:45', '0000-00-00 00:00:00', NULL),
(7, '1981-06-17', '2016-03-26 02:39:52', '0000-00-00 00:00:00', NULL),
(8, '2016-04-07', '2016-04-01 10:31:12', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `Book_title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `Book_title`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Data Structure', '2016-03-12 06:45:03', '2016-03-15 15:03:20', '0000-00-00 00:00:00'),
(2, 'ANSI C', '2016-03-12 07:19:14', '2016-03-15 15:03:40', '0000-00-00 00:00:00'),
(3, 'Java', '2016-03-12 07:29:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Algorithm Part 1', '2016-03-12 07:52:48', '2016-03-26 14:29:38', '0000-00-00 00:00:00'),
(5, 'Database', '2016-03-12 08:20:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Physics', '2016-03-12 16:20:26', '0000-00-00 00:00:00', '2016-04-01 10:46:33'),
(12, 'Mathematics', '2016-03-12 19:04:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Chemistry', '2016-03-12 19:05:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Bio-Chemistry', '2016-03-13 05:42:31', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Pharmacy', '2016-03-15 04:53:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'microbiology', '2016-03-15 04:56:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'Abir', '2016-03-15 15:04:00', '0000-00-00 00:00:00', '2016-03-15 15:34:01');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `hobby` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `hobby`) VALUES
(1, 'Cricket,Football,Gardening '),
(2, 'Cricket,Football,Gardening'),
(3, 'Cricket,Gardening'),
(5, 'Cricket'),
(7, 'Cricket,Football,Gardening'),
(8, 'Cricket,Football,Gardening');

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE `mobile` (
  `id` int(11) NOT NULL,
  `mobile_title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`id`, `mobile_title`, `created`, `updated`, `deleted_at`) VALUES
(1, 'Sony', '0000-00-00 00:00:00', '2016-03-27 06:43:04', '2016-04-01 10:50:30'),
(2, 'Sony', '0000-00-00 00:00:00', '2016-03-27 06:50:22', '2016-04-01 10:50:32'),
(3, 'Firefox', '2016-03-13 04:05:06', '0000-00-00 00:00:00', '2016-04-01 10:50:33'),
(4, 'Symphony w82', '2016-03-13 04:07:57', '0000-00-00 00:00:00', '2016-04-01 10:50:34'),
(8, 'Sony Xperia', '2016-03-21 18:58:18', '0000-00-00 00:00:00', '2016-04-01 10:50:35'),
(9, 'hgfhqgsf', '2016-03-26 14:58:46', '0000-00-00 00:00:00', '2016-04-01 10:50:43'),
(10, 'hgfhqgsf', '2016-03-26 14:59:41', '0000-00-00 00:00:00', NULL),
(11, 'hgfhqgsf', '2016-03-26 15:00:45', '0000-00-00 00:00:00', NULL),
(12, 'hgfhqgsf', '2016-03-26 15:01:00', '0000-00-00 00:00:00', NULL),
(13, 'hgfhqgsf', '2016-03-26 15:01:33', '0000-00-00 00:00:00', NULL),
(14, 'hgfhqgsf', '2016-03-26 15:03:23', '0000-00-00 00:00:00', '2016-04-01 10:51:03');

-- --------------------------------------------------------

--
-- Table structure for table `multiple_check`
--

CREATE TABLE `multiple_check` (
  `id` int(11) NOT NULL,
  `lang` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `multiple_check`
--

INSERT INTO `multiple_check` (`id`, `lang`, `created`, `updated`, `deleted`) VALUES
(4, 'C,C++,Java,python,C#', '2016-03-27 07:39:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'C,C++,Java,python,PHP,C#', '2016-03-27 08:00:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'C,C++,Java,python,PHP', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'C,C++,Java,python,PHP', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'C#', '2016-03-27 09:01:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profilepic`
--

CREATE TABLE `profilepic` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `imagename` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted_at` datetime NOT NULL,
  `flag` datetime(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepic`
--

INSERT INTO `profilepic` (`id`, `username`, `imagename`, `created`, `updated`, `deleted_at`, `flag`) VALUES
(2, 'Sohel Rana', '1459653681_4.jpg', '2016-04-03 05:21:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-04 12:41:21.0'),
(3, 'Arifur Rahman', '1459653693_3.jpg', '2016-04-03 05:21:33', '2016-04-03 06:20:57', '0000-00-00 00:00:00', '2016-04-03 08:34:37.0'),
(4, 'Sadman Sadik', '1459657266_1.jpg', '2016-04-03 05:21:53', '2016-04-03 06:21:06', '0000-00-00 00:00:00', '2016-04-04 06:17:00.0'),
(5, 'Md. Anwarul islam Abir', '1459743065_02.jpg', '2016-04-04 06:11:05', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-04-04 06:11:50.0');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `name`, `gender`, `city`, `email`, `created`, `updated`, `deleted`) VALUES
(3, 'abir', 'male', 'dhaka', 'a@gmail.com', '2016-03-29 06:15:18', '2016-03-29 08:32:09', '0000-00-00 00:00:00'),
(4, 'rana', 'male', 'dhaka', 'a@gmail.com', '2016-03-29 06:20:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Md. Anwarul Islam Abir', 'male', 'dhaka', 'abir@gmail.com', '2016-03-29 06:21:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Nupur', 'female', 'dhaka', 'nupur@gmail.com', '2016-03-29 07:33:18', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Arifa', 'female', 'rajshahi', 'arif@a.c', '2016-03-29 07:49:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'a', 'male', 'dhaka', 'a@c.d.s', '2016-03-31 05:26:55', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'abir', 'male', 'dhaka', 'a@g.com', '2016-03-31 07:16:02', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_info`
--
ALTER TABLE `admin_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile`
--
ALTER TABLE `mobile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `multiple_check`
--
ALTER TABLE `multiple_check`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepic`
--
ALTER TABLE `profilepic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_info`
--
ALTER TABLE `admin_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `mobile`
--
ALTER TABLE `mobile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `multiple_check`
--
ALTER TABLE `multiple_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `profilepic`
--
ALTER TABLE `profilepic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
