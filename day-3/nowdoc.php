<?php
/**
 * Created by PhpStorm.
 * User: Abir
 * Date: 2/14/2016
 * Time: 8:11 AM
 */

$str = <<< 'EOD'
Example of String
Sanning Multiple Lines
Using nowdoc syntax.
EOD;

echo $str;

$name = 'MyName';

echo <<<'EOD'

My name is "$name". I am printing some.
Now, I am printing some.
This should print a capital 'A': \x41
EOD;
?>