<?php

$var = '';

if (isset($var)) {
    echo "this variable is set so i will print";
}

$a = "test";
$b = "anothertest";
echo "</br>";

var_dump(isset($a));
echo "a </br>";
var_dump(isset($a, $b));
echo "a, b</br>";

unset($a);

var_dump(isset($a));
echo "unset a </br>";
var_dump(isset($a, $b));
echo "after unset a then b </br>";

$foo = null;
var_dump(isset($foo));

