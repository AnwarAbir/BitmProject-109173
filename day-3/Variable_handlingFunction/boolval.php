<?php
/**
 * Created by PhpStorm.
 * User: Abir
 * Date: 2/22/2016
 * Time: 9:52 PM
 */

$var = 0;

if(boolval($var)){
    echo $var;
}else{
    echo "fine";
}

echo "<Br/>";

echo '0:        '.(boolval(0) ? 'true' : 'false')."\n<Br/>";
echo '42:       '.(boolval(42) ? 'true' : 'false')."\n<Br/>";
echo '0.0:      '.(boolval(0.0) ? 'true' : 'false')."\n<Br/>";
echo '4.2:      '.(boolval(4.2) ? 'true' : 'false')."\n<Br/>";
echo '"":       '.(boolval("") ? 'true' : 'false')."\n<Br/>";
echo '"string": '.(boolval("string") ? 'true' : 'false')."\n<Br/>";
echo '"0":      '.(boolval("0") ? 'true' : 'false')."\n<Br/>";
echo '"1":      '.(boolval("1") ? 'true' : 'false')."\n<Br/>";
echo '[1, 2]:   '.(boolval([1, 2]) ? 'true' : 'false')."\n<Br/>";
echo '[]:       '.(boolval([]) ? 'true' : 'false')."\n<Br/>";
echo 'stdClass: '.(boolval(new stdClass) ? 'true' : 'false')."\n";