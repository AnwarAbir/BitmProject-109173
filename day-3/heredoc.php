<?php
/**
 * Created by PhpStorm.
 * User: Abir
 * Date: 2/14/2016
 * Time: 7:52 AM
 */

$str = <<<EOD
Example of string
spanning multiple lines
using heredoc syntax.
EOD;

echo $str;

/* More complex example, with variables. */

$name = 'MyName';

echo <<<EOT

My name is "$name". I am printing some.
Now, I am printing some.
This should print a capital 'A': \x41
EOT;
?>