<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 3/6/2016
 * Time: 11:22 AM
 */

namespace app\bookHistory;

class book {

    public $bookName;
    public $authorName;

    function __construct($bookName, $authorName) {
        $this->bookName = $bookName;
        $this->authorName = $authorName;
//        echo "$this->bookName";
    }

    function getName() {
        return $this->bookName;
    }

    function getAuthor() {
        return $this->authorName;
    }

    function index() {
        return $Array = array($this->bookName, $this->authorName);
    }

}
