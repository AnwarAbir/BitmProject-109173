<?php include_once '../vendor/autoload.php';
use App\seip_109173\lang\lang;

?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $lang = $_GET['lang'];
        $comma_separated = implode(",", $lang);
        $_GET['lang'] = $comma_separated;
        
        $updateLang  = new lang();
        $updateLang->setValues($_GET);
        $updateLang->update();
        ?>
    </body>
</html>