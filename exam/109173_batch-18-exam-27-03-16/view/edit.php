<!DOCTYPE html>
<?php
include_once '../vendor/autoload.php';

use App\seip_109173\lang\lang;
?>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $update = new lang();
        $update->setValues($_GET);
        $language = $update->show();
        $languageArray = explode(",", $language['lang']);
//        print_r($languageArray);
        ?>
        <fieldset>
            <legend>Edit Info</legend>
            <form action="update.php" method="get">
                <table>
                    <tr>
                        <td>
                            <strong>Enter Favorite Programming Language:</strong><br>
                            <input type="checkbox"   name="lang[]" value="C" <?php if (in_array('C', $languageArray)) { ?> checked="checked" <?php } ?> />C<br>
                            <input type="checkbox"  name="lang[]" value="C++"  <?php if (in_array('C++', $languageArray)) { ?> checked="checked" <?php } ?>/>C++<br>
                            <input type="checkbox"  name="lang[]" value="Java" <?php if (in_array('Java', $languageArray)) { ?> checked="checked" <?php } ?> />Java<br>
                            <input type="checkbox"  name="lang[]" value="python"  <?php if (in_array('python', $languageArray)) { ?> checked="checked" <?php } ?>/>Python<br>
                            <input type="checkbox"  name="lang[]" value="PHP"  <?php if (in_array('PHP', $languageArray)) { ?> checked="checked" <?php } ?>/>PHP<br>
                            <input type="checkbox"  name="lang[]" value="C#"  <?php if (in_array('C#', $languageArray)) { ?> checked="checked" <?php } ?>/>C#
                            <input type="hidden"     name="id" value="<?php echo $language['id'] ?>" /> <br>
                            <input type="submit" value="Update" /></td>
                    </tr>
                </table>
            </form>
        </fieldset>
    </body>
</html>
