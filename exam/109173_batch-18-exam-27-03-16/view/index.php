<?php include_once '../vendor/autoload.php'; ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <a href="create.php">Create</a><br/>
        <?php

        use App\seip_109173\lang\lang;

$allLanguageOb = new lang();
        $allLanguage = $allLanguageOb->index();

//        echo "<pre>";
//        print_r($allLanguage);
//        echo "<pre>";
        ?>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Fav Language</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            <?php foreach ($allLanguage as $language) { ?>
                <tr>
                    <td><?php echo $language['id'];?></td>
                    <td><?php echo $language['lang'];?></td>
                    <td><?php echo $language['created'];?></td>
                    <td>
                        <a href="show.php?id=<?php echo $language['id'];?>">View Details</a> |
                        <a href="edit.php?id=<?php echo $language['id'];?>">Update</a> |
                        <a href="delete.php?id=<?php echo $language['id'];?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </body>
</html>
