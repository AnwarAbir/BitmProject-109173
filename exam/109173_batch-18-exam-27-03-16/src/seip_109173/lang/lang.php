<?php

namespace App\seip_109173\lang;

class lang {

    public $id = '';
    public $lang = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Can not connect to server');
        mysql_selectdb("109173_project", $conn) or die("Can not connect to DB");
    }

    public function setValues($array = array()) {
        if (array_key_exists('id', $array)) {
            $this->id = $array['id'];
        }
        if (array_key_exists('lang', $array)) {
            $this->lang = $array['lang'];
        }
    }

    public function store() {
        $query = "INSERT INTO `109173_project`.`multiple_check` (`id`, `lang`, `created`) VALUES (NULL, '" . $this->lang . "', '" . date("Y-m-d h:i:s") . "')";
        session_start();
        if (mysql_query($query)) {
            $_SESSION['msg'] = "Succesfully Inserted";
            ob_clean();
            ob_flush();
            header("location:index.php");
        }
    }

    public function index() {
        $allLang = array();
        $query = "SELECT * FROM multiple_check";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $allLang [] = $row;
        }
        return $allLang;
    }

    public function show() {
        $query = "SELECT * FROM `multiple_check` WHERE `id` =" . $this->id;
        $result = mysql_query($query);
        return mysql_fetch_assoc($result);
    }

    public function delete() {
        $query = "DELETE FROM `109173_project`.`multiple_check` WHERE `multiple_check`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            header("location:index.php");
        }
    }

    public function update() {
        $query = "UPDATE `109173_project`.`multiple_check` SET `lang` = '" . $this->lang . "' WHERE `multiple_check`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            header("location:index.php");
        }
    }

}
