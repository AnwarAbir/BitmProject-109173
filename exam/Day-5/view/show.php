<?php
include_once '../vendor/autoload.php';
session_start();

use App\projectClass\userInfo;

$detialsViewObj = new userInfo();
$detialsViewObj->setId($_GET['id']);
$DetailsUserData = $detialsViewObj->viewDtails();

//echo "<pre>";
//print_r($DetailsUserData);
//echo "<pre>";
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>                                    
            <tr>
                <td><?php echo $DetailsUserData['id']; ?></td>
                <td><?php echo $DetailsUserData['name']; ?></td>
                <td><?php echo $DetailsUserData['created']; ?></td>
                <td>
                    <a href="index.php?id=<?php echo $DetailsUserData['id']; ?>">Update</a> |
                    <a href="delete.php?id=<?php echo $DetailsUserData['id']; ?>">Delete</a> |
                </td>
            </tr>
        </table>
        <br/>

        <a href="index.php">List View</a>
    </body>
</html>