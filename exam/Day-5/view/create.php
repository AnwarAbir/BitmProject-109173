<?php
include_once '../vendor/autoload.php';
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <fieldset>
            <legend>Add New User</legend>
            <form action="store.php" method="get">
                <label for="uname">User Name</label><br/>
                <input type="text" name="name" id="uname" placeholder="username"/><br/>
                <input type="submit" value="Save"/>
                <input type="reset" value="Reset"/>
            </form>
        </fieldset>
        <?php
        if (isset($_SESSION) && !empty($_SESSION)) {
            echo $_SESSION['msg'];
            session_unset();
            session_destroy();
        }
        ?>
        <Br/>
        <Br/>
        <a href="index.php">Full User List</a>
    </body>
</html>
