<?php
include_once '../vendor/autoload.php';
session_start();
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <a href="create.php">Create</a><Br/><Br/>
        <?php
        if (isset($_SESSION) && !empty($_SESSION)) {
            echo $_SESSION['msg'];
            session_unset();
            session_destroy();
        }

        use App\projectClass\userInfo;

        $allUserObj = new userInfo();
        $allUserData = $allUserObj->listView();

//        echo "<pre>";
//        print_r($allUserData);
//        echo "<pre>";
        ?>

        <table border="1">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            <?php foreach ($allUserData as $oneuserData) { ?>                                      
                <tr>
                    <td><?php echo $oneuserData['id']; ?></td>
                    <td><?php echo $oneuserData['name']; ?></td>
                    <td><?php echo $oneuserData['created']; ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $oneuserData['id']; ?>">View Details</a> |
                        <a href="index.php?id=<?php echo $oneuserData['id']; ?>">Update</a> |
                        <a href="delete.php?id=<?php echo $oneuserData['id']; ?>">Delete</a> |
                    </td>
                </tr>
            <?php } ?>
        </table>
    </body>
</html>