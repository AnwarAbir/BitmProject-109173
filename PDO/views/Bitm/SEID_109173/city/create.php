<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="js.js" type="text/javascript"></script>
        <title></title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';
        ?>
        <fieldset>
            <legend>Input Information</legend>
            <form action="store.php" method="get" name="myForm">
                <label for="uname">Name</label><br/>
                <input type="text" name="name" id="uname" placeholder="username"/><br/>
                <label for="mail">Email</label><br/>
                <input type="email" name="email" id="mail" placeholder="example@email.com"/><br/>
                <label for="gen">Gender</label><br/>
                <input type="radio" name="gender" value="male" id="gen" />Male<br/>
                <input type="radio" name="gender" value="female" id="gen"/>Female<br/>
                <input type="radio" name="gender" value="others" id="gen"/>Others<br/>
                <label>Select City</label><br/>
                <select name="city">
                    <option value="dhaka">Dhaka</option>
                    <option value="chittagong">Chittagong</option>
                    <option value="sylhet">Sylhet</option>
                    <option value="rajshahi">Rajshahi</option>
                    <option value="khulna">Khulna</option>
                    <option value="barisal">Barisal</option>
                    <option value="rangpur">Rangpur</option>
                </select><br/>
                <input type="submit" value="Save" onclick="validateForm()"/>
                <input type="reset" value="Reset"/>
            </form>
        </fieldset>
    </body>
</html>
