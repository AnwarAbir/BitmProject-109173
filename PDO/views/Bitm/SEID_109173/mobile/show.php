<?php include_once '../../../../vendor/autoload.php'; ?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
     <?php
        include_once '../menu/menu.php';

//        print_r($_GET);

        use App\Bitm\SEID_109173\mobile\mobile;
        use App\Bitm\SEID_109173\utility\utility;

$mobileDetailsOb = new mobile();
        $mobileDetailsOb->setId($_GET['id']);
        $oneMobileData = $mobileDetailsOb->show();

        $debug = new utility();
//        $debug->debug($oneMobileData);
        ?>
        <table border="1">
            <tr>
                <th>SI</th>
                <th>Mobile Name</th>
                <th>Created Date</th>
                <th>Last Updated</th>
                <th>Action</th>
            </tr>
            <tr>
                <td><?php echo $oneMobileData['id']; ?></td>
                <td><?php echo $oneMobileData['mobile_title']; ?></td>
                <td><?php echo $oneMobileData['created']; ?></td>
                <td><?php echo $oneMobileData['updated']; ?></td>
                <td>
                    <a href="update.php?id=<?php echo $oneMobileData['id']; ?>">Update</a> |
                    <a href="trash.php?id=<?php echo $oneMobileData['id']; ?>">Delete</a>
                </td>
            </tr>
        </table>
    </body>
</html>
