<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\gender\gender;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';
        $allHumanObj = new gender();
        $allHumansData = $allHumanObj->index();

//        $debug = new utility();
//        $debug->debug($allHumansData);
        $counter = 0;
        ?>

        <table border="1">
            <tr>
                <th>Serial</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            <?php foreach ($allHumansData as $oneHumanData) { ?>
                <tr>
                    <td><?php echo ++$counter; ?></td>
                    <td><?php echo $oneHumanData['name']; ?></td>
                    <td><?php echo $oneHumanData['email']; ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $oneHumanData['id']; ?>">View Details</a> |
                        <a href="edit.php?id=<?php echo $oneHumanData['id']; ?>">Update</a> |
                        <a href="trash.php?id=<?php echo $oneHumanData['id']; ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>

        </table>
    </body>
</html>
