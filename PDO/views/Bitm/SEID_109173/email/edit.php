<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\gender\gender;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';
        $update = new gender();
        $update->setId($_GET['id']);
        $dataUpadte = $update->show();


//        $debug = new utility();
//        $debug->debug($dataUpadte);
        ?>
        <fieldset>
            <legend>Update Information</legend>
            <form action="update.php" method="get">
                <label for="uname">Name</label><br/>
                <input type="text" name="name" id="uname" value="<?php echo $dataUpadte['name']; ?>"/><br/>
                <label for="mail">Email</label><br/>
                <input type="email" name="email" id="mail"  value="<?php echo $dataUpadte['email']; ?>"/><br/>
                <label for="gen">Gender</label><br/>
                <input type="radio" name="gender" value="male" id="gen" <?php if ($dataUpadte['gender'] == 'male') { ?>checked="<?php echo $dataUpadte['gender']; } ?>"/>Male<br/>
                <input type="radio" name="gender" value="female" id="gen" <?php if ($dataUpadte['gender'] == 'female') { ?> checked="<?php echo $dataUpadte['gender']; } ?>"/>Female<br/>
                <input type="radio" name="gender" value="others" id="gen" <?php if ($dataUpadte['gender'] == 'others') {?>checked="<?php   echo $dataUpadte['gender']; } ?>"/>Others<br/>
                <label>Select City</label><br/>
                <select name="city">
                    <option value="dhaka" <?php if ($dataUpadte['city'] == 'dhaka') {?>selected="dhaka"<?php }?>>Dhaka</option>
                    <option value="chittagong" <?php if ($dataUpadte['city'] == 'chittagong') {?>selected<?php }?>>Chittagong</option>
                    <option value="sylhet" <?php if ($dataUpadte['city'] == 'sylhet') {?>selected<?php }?>>Sylhet</option>
                    <option value="rajshahi" <?php if ($dataUpadte['city'] == 'rajshahi') {?>selected<?php }?>>Rajshahi</option>
                    <option value="khulna" <?php if ($dataUpadte['city'] == 'khulna') {?>selected<?php }?>>Khulna</option>
                    <option value="barisal" <?php if ($dataUpadte['city'] == 'barisal') {?>selected<?php }?>>Barisal</option>
                    <option value="rangpur" <?php if ($dataUpadte['city'] == 'rangpur') {?>selected<?php }?>>Rangpur</option>
                </select><br/>
                <input type="hidden" name="id" value="<?php echo $dataUpadte['id']; ?>"/>
                <input type="submit" value="Save"/>
                <input type="reset" value="Reset"/>
            </form>
        </fieldset>
    </body>
</html>
