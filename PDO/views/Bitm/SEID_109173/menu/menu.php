<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<link rel="stylesheet" href="styles.css">-->       
        <link rel="stylesheet" href="../menu/styles.css">

        <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="script.js"></script>
        <title>109173</title>
    </head>
    <body>

        <div id='cssmenu'>
            <ul>
                <li><a href='../../../../index.php'><span>Home</span></a></li>
                <li class='has-sub'><a href='../Book/index.php'><span>Book</span></a>
                    <ul>
                        <li class='has-sub'><a href='../Book/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../Book/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../Book/trshedItem.php'><span>Deleted Items</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                    </ul>
                </li>
                <li class='has-sub'><a href='../mobile/index.php'><span>Mobile</span></a>
                    <ul>
                        <li class='has-sub'><a href='../mobile/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../mobile/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../mobile/trashedItems.php'><span>Deleted items</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                    </ul>
                </li>
                <li class='has-sub'><a href='../birthday/index.php'><span>Birthday</span></a>
                    <ul>
                        <li class='has-sub'><a href='../birthday/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../birthday/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../birthday/trashedItems.php'><span>Deleted items</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                    </ul>
                </li>
                <li class='has-sub'><a href='../hobbies/index.php'><span>Hobbies</span></a>
                    <ul>
                        <li class='has-sub'><a href='../hobbies/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../hobbies/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../hobbies/trashedItems.php'><span>Deleted items</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                    </ul>
                </li>
                <li class='has-sub'><a href='../gender/index.php'><span>Gender</span></a>
                    <ul>
                        <li class='has-sub'><a href='../gender/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../gender/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../gender/trashedItems.php'><span>Deleted items</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                    </ul>
                </li>
                <li class='has-sub'><a href='../profilePic/index.php'><span>ProfilePic</span></a>
                    <ul>
                        <li class='has-sub'><a href='../profilePic/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../profilePic/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../profilePic/trashedItems.php'><span>Deleted items</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                    </ul>
                </li>
                <li class='has-sub'><a href='../city/index.php'><span>City</span></a>
                    <ul>
                        <li class='has-sub'><a href='../city/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../city/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../city/trashedItems.php'><span>Deleted items</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                    </ul>
                </li>
                <li class='has-sub'><a href='../email/index.php'><span>Email</span></a>
                    <ul>
                        <li class='has-sub'><a href='../email/create.php'><span>Create</span></a>
                            <!--<ul>
                                <li><a href='#'><span>Sub Product</span></a></li>
                                <li class='last'><a href='#'><span>Sub Product</span></a></li>
                            </ul>
                            -->
                        </li>
                        <li class='has-sub'><a href='../email/show.php'><span>Show</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                        <li class='has-sub'><a href='../email/trashedItems.php'><span>Deleted items</span></a>
                            <!--                            
                            <ul>
                                                            <li><a href='#'><span>Sub Product</span></a></li>
                                                            <li class='last'><a href='#'><span>Sub Product</span></a></li>
                                                        </ul>-->
                        </li>
                    </ul>
                </li>
                <li><a href='#'><span>About</span></a></li>
                <li class='last'><a href='#'><span>Contact</span></a></li>
                <li><a href='../../../../logOut.php'><span>LogOut</span></a></li>
            </ul>
        </div>
    </body>
</html>
