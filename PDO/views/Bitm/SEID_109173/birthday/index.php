<?php include_once '../../../../vendor/autoload.php'; ?>
<?php
session_start();
if (isset($_SESSION) && !empty($_SESSION)) {
//    echo '<script type="text/javascript">alert("' . $_SESSION['msg'] . '"); </script>';
    echo $_SESSION['msg'];
    session_unset();
    session_destroy();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL ^ E_DEPRECATED);
        include_once '../menu/menu.php';

        use App\Bitm\SEID_109173\utility\utility;
        use App\Bitm\SEID_109173\birthday\birthday;

$storeBdate = new birthday();
        $allBirthday = $storeBdate->index();


        $debug = new utility();
//        $debug->debug($allBirthday);
//        die();
        $counter = 0;
        ?>

        <table border="1">
            <tr>
                <th>SI</th>
                <th>Birthday</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            <?php foreach ($allBirthday as $oneBirthday) {?>
                <tr>
                    <td><?php echo ++$counter; ?></td>
                    <td><?php echo $oneBirthday['bday'] ?></td>
                    <td><?php echo $oneBirthday['created'] ?></td>
                    <td>
                        <a href="show.php?id=<?php echo $oneBirthday['id'] ?>">View Details</a> |
                        <a href="edit.php?id=<?php echo $oneBirthday['id'] ?>">Update</a> |
                        <a href="trash.php?id=<?php echo $oneBirthday['id'] ?>">Delete</a>
                    </td>
                </tr>      
            <?php } ?>
        </table>
    </body>
</html>
