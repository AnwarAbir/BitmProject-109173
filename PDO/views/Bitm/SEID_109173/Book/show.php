<?php include_once '../../../../vendor/autoload.php'; ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Book</title>
    </head>
    <body>
        <?php

        use App\Bitm\SEID_109173\Book\book;

include_once '../menu/menu.php';

        $showObj = new book();
        $showObj->setId($_GET['id']);
        $bookDetails = $showObj->show();
//        print_r($bookDetails);
//        die();
        ?>

        <table border ="1">
            <tr>
                <th>Serial No</th>
                <th>Book Title</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            <?php if (isset($bookDetails) && !empty($bookDetails)) { ?>
                <tr>
                    <td><?php echo $bookDetails['id']; ?></td>
                    <td><?php echo $bookDetails['Book_title']; ?></td>
                    <td><?php echo $bookDetails['created']; ?></td>
                    <td>
                        <a href="edit.php?id=<?php echo $bookDetails['id'] ?>">Update</a> |
                        <a href="delete.php?id=<?php echo $bookDetails['id'] ?>">Delete</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </body>
</html>
