<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\Book\book;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
        include_once '../menu/menu.php';


        $oneBookObj = new book();
        $oneBookObj->setId($_GET['id']);
        $bookDetails = $oneBookObj->show();
//        print_r($bookDetails);
        ?>
        <fieldset>
            <legend>Update Data</legend>
            <form action="update.php" method="get">
                <input type="text" name="title" value="<?php echo $bookDetails['Book_title']; ?>"/><br/>
                <input type="submit" value="Update"/>
                <input type="hidden" name="id" value="<?php echo $bookDetails['id']; ?>"/>
            </form>
        </fieldset>
    </body>
</html>
