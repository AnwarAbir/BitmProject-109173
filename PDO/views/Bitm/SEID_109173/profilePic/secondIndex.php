<?php
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\profilePic\profilePic;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        error_reporting(E_ALL ^ E_DEPRECATED);
        include_once '../menu/menu.php';


        $showProfile = new profilePic();
        $showProfile->setId($_GET['id']);
        $oneProfile = $showProfile->show();


        $debug = new utility();
//        $debug->debug($oneProfile);
//        die();
        ?>

        <table border="1">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Profile Pic</th>
                <th>created date</th>
                <th>Action</th>
            </tr>
            <tr>
            
                <td><?php echo $oneProfile['id']; ?></td>
                <td><?php echo $oneProfile['username']; ?></td>
                <td><img src="../../../../image/<?php echo $oneProfile['imagename']; ?>"></td>
                <td><?php echo $oneProfile['created']; ?></td>
                <td>
                    <a href="proPic.php<?php echo $oneProfile['id']; ?>">Active</a> |
                    <a href="edit.php?id=<?php echo $oneProfile['id']; ?>">Update</a> |
                    <a href="trash.php?id=<?php echo $oneProfile['id']; ?>">Delete</a>
                </td>
            </tr>

        </table>
    </body>
</html>
