<?php
error_reporting(E_ALL ^ E_DEPRECATED);
include_once '../../../../vendor/autoload.php';

use App\Bitm\SEID_109173\profilePic\profilePic;
use App\Bitm\SEID_109173\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Book</title>
    </head>
    <body>
        <?php
        include_once '../menu/menu.php';

        echo "<h3 style = 'text-allign:center'>A good Book is like a 100 friends and a good friend is like a library</h3><Br/><Br/>";


        $listViewObj = new profilePic();
        $allProfiles = $listViewObj->trashedItems();
//        echo "<pre>";
//        print_r($allProfiles);
//        echo "<pre>";
//        die();
        $counter = 0;
        ?>
        <!--<a href="create.php">Create Page</a>-->

        <table border="1">
            <tr>
                <th>SI</th>
                <th>Title</th>
                <th>Created Date</th>
                <th>Action</th>
            </tr>
            <?php foreach ($allProfiles as $oneProfile) { ?>
                <tr>
                    <td><?php echo ++$counter; ?></td>
                    <td><?php echo $oneProfile['username'] ?></td>
                    <td><img src="<?php echo "../../../../image/" . $oneProfile['imagename'] ?>" width="200" height="140"></td>
                    <td>
                        <a href="show.php?id=<?php echo $oneProfile['id'] ?>">View Details</a> |
                        <a href="restore.php?id=<?php echo $oneProfile['id'] ?>">Restore</a> |
                        <a href="delete.php?id=<?php echo $oneProfile['id'] ?>">Delete Permanently</a>
                    </td>
                </tr>
                <?php
            }
            if (isset($_SESSION) && !empty($_SESSION)) {
//                echo $_SESSION['msg'];
                echo '<script type="text/javascript">alert("' . $_SESSION['msg'] . '"); </script>';
                session_unset();
                session_destroy();
            }
            ?>
        </table>
    </body>
</html>
