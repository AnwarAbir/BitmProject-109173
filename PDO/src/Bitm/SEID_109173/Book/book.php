<?php

/**
 * Description of book
 *
 * @author Web App Develop PHP
 */

namespace App\Bitm\SEID_109173\Book;

use PDO;

class book {

    public $id = "";
    public $book_title = "";
    public $user = 'root';
    public $pass = '';
    public $conn;

    function __construct() {
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=109173_project', $this->user, $this->pass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    function setBook_title($book_title) {
        $this->book_title = $book_title;
    }

    function setId($id) {
        return $this->id = $id;
    }

    public function getTitle() {
        return $this->book_title;
    }

    public function store() {
        $query = "INSERT INTO `109173_Project`.`book` (`id`, `Book_title`, `created`) VALUES (NULL, '" . $this->book_title . "', '" . date("Y-m-d H:i:s") . "')";
        session_start();
        if (empty($this->book_title)) {
            $_SESSION["msg"] = "Please Enter Some data";
            header('Location:create.php');
        } else {
            if (mysql_query($query)) {
                $_SESSION["msg"] = "Succesful Added";
            } else {
                $_SESSION["msg"] = "Sorry Something went wrong";
            }
        }

        header('Location:index.php');
    }

    public function index() {
        $allData = array();
        $query = "SELECT * FROM `book` WHERE  deleted_at = 0 ORDER BY `book`.`Book_title` ASC";

        $getrow = $this->conn->prepare($query);
        $getrow->execute();
        return $getrow->fetchAll();
    }

    public function show() {
        $statement = $this->conn->prepare("SELECT * FROM `book` WHERE `id`  = :id");
        $statement->execute(array(':id' => "$this->id"));
        return $statement->fetch();
    }

    public function deleteBook($id = '') {
        $this->id = $id;
        $query = "DELETE FROM `book` WHERE `id` =" . $this->id;
        return mysql_query($query);
    }

    public function update() {
        try {
            ob_start();
            session_start();
            $query = "UPDATE `109173_project`.`book` SET `Book_title` = '" . $this->book_title . "', `updated`='" . date("Y-m-d H:i:s") . "' WHERE `book`.`id` =" . $this->id;

            // Prepare statement
            $stmt = $this->conn->prepare($query);

            // execute the query
            $stmt->execute();
            $_SESSION['msg'] = "Succesfully Updated";
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'index.php';</script>";
        } catch (PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }

        $conn = null;
    }

    public function trash() {
        ob_start();
        session_start();
        $query = "UPDATE `109173_project`.`book` SET  `deleted_at`='" . date("Y-m-d H:i:s") . "' WHERE `book`.`id` =" . $this->id;
        $statement = $this->conn->prepare($query);
        $statement->execute();
        $_SESSION['msg'] = "Succesfully Deleted";
        ob_clean();
        ob_flush();
        @header('Location: index.php');
        echo "<script>window.location = 'index.php';</script>";
//        return $statement->fetch();
    }

    public function trashedItem() {
        $allData = array();
        $query = "SELECT * FROM `book` WHERE  deleted_at != 0";
        $getrow = $this->conn->prepare($query);
        $getrow->execute();
        return $getrow->fetchAll();
    }

    public function restore() {
        $query = "UPDATE `109173_project`.`book` SET  `deleted_at`='NULL' WHERE `book`.`id` =" . $this->id;
//        return mysql_query($query);
        $getrow = $this->conn->prepare($query);
        return $getrow->execute();
    }

//UPDATE `109173_project`.`book` SET `deleted_at` = NULL WHERE `book`.`id` = 27;
}
