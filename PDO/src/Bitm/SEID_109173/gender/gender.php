<?php

namespace App\Bitm\SEID_109173\gender;

class gender {

    public $id = '';
    public $name = '';
    public $email = '';
    public $gender = '';
    public $city = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Can not connect to server');
        mysql_selectdb("109173_project", $conn) or die("Can not connect to DB");
    }

    public function setValues($array = array()) {
        $this->name = $array['name'];
        $this->email = $array['email'];
        $this->gender = $array['gender'];
        $this->city = $array['city'];
        return $this;
    }

    public function getValues() {
        echo $this->name . " " . $this->email . " " . $this->gender . " " . $this->city;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function store() {
        $query = "INSERT INTO `109173_project`.`user_info` (`id`, `name`, `gender`, `city`, `email`, `created`) VALUES (NULL, '" . $this->name . "', '" . $this->gender . "', '" . $this->city . "', '" . $this->email . "', '" . date("Y-m-d h:i:s") . "')";
        session_start();
        if (!empty($this->name) && !empty($this->email) && !empty($this->city) && !empty($this->gender)) {
            if (mysql_query($query)) {
                $_SESSION['msg'] = "Succesfully new Data Added";
                ob_clean();
                ob_flush();
                header("location:index.php");
            } else {
                $_SESSION['msg'] = "something went wrong";
            }
        } else {
            ob_clean();
            ob_flush();
            header("location:create.php");
        }
    }

    public function index() {
        $allData = array();
        $query = "SELECT * FROM `user_info` WHERE `deleted` = 0";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $allData [] = $row;
        }

        return $allData;
    }

    public function show() {
        $query = "SELECT * FROM `user_info` WHERE `id` = " . $this->id;
        $result = mysql_query($query);
        return mysql_fetch_assoc($result);
    }

    public function trash() {
        $query = "UPDATE `109173_project`.`user_info` SET `deleted` = '" . date("Y-m-d h:i:s") . "' WHERE `user_info`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'index.php';</script>";
        } else {
            echo 'somthing Went Wrong';
        }
    }

    public function trashedItems() {
        $allData = array();
        $query = "SELECT * FROM `user_info` WHERE `deleted` != 0";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $allData [] = $row;
        }

        return $allData;
    }

    public function delete() {
        $query = "DELETE FROM `109173_project`.`user_info` WHERE `user_info`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'trashedItems.php';</script>";
        } else {
            echo 'Something went wrong';
        }
    }

    public function restore() {
        $query = "UPDATE `109173_project`.`user_info` SET `deleted` = '" . 0 . "' WHERE `user_info`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'index.php';</script>";
        } else {
            echo 'somthing Went Wrong';
        }
    }

    public function update() {
        $query = "UPDATE `user_info` SET `name`='" . $this->name . "',`gender`='" . $this->gender . "',`city`='" . $this->city . "',`email`='" . $this->email . "',`updated`='".  date("Y-m-d h:i:s")."' WHERE `id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'index.php';</script>";
        } else {
            echo 'somthing Went Wrong';
        }
    }

}
