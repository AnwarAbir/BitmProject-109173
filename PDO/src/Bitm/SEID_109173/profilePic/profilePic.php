<?php

namespace App\Bitm\SEID_109173\profilePic;

/**
 * Description of profilePic
 *
 * @author Abir
 */
class profilePic {

    public $id = '';
    public $name = '';
    public $image = '';

    public function __construct() {
        $conn = mysql_connect('localhost', 'root', '') or die('Can not connect to server');
        mysql_selectdb("109173_project", $conn) or die("Can not connect to DB");
    }

    public function setValues($array = array()) {
        if (array_key_exists('name', $array)) {
            $this->name = $array['name'];
        }
        if (array_key_exists('image', $array)) {
            $this->image = $array['image'];
        }
        return $this;
    }

    public function getValues() {
        echo $this->name . " " . $this->email . " " . $this->gender . " " . $this->city;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function store() {
        $query = "INSERT INTO `profilepic` (`id`, `username`, `imagename`, `created`) VALUES (NULL, '" . $this->name . "', '" . $this->image . "', '" . date("Y-m-d h:i:s") . "')";
        session_start();
        if (!empty($this->name) && !empty($this->image)) {
            if (mysql_query($query)) {
                $_SESSION['msg'] = "Succesfully new Data Added";
                ob_clean();
                ob_flush();
                header("location:index.php");
            } else {
                $_SESSION['msg'] = "something went wrong";
            }
        } else {
            ob_clean();
            ob_flush();
            header("location:create.php");
        }
    }

    public function index() {
        $allData = array();
        $query = "SELECT * FROM `profilepic` WHERE `deleted_at` = 0";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $allData [] = $row;
        }

        return $allData;
    }

    public function show() {
        $query = "SELECT * FROM `profilepic` WHERE `id` = " . $this->id;
        $result = mysql_query($query);
        $query1 = "UPDATE `109173_project`.`profilepic` SET `flag` = '" . date("Y-m-d h:i:s") . "' WHERE `profilepic`.`id` = " . $this->id;
        mysql_query($query1);
//        $queryUp1 = "UPDATE `109173_project`.`profilepic` SET `flag` = '1' WHERE `id` = " . $this->id;
//        mysql_query("UPDATE `109173_project`.`profilepic` SET `flag` = '0' WHERE 1 ");
//        mysql_query($queryUp1);
//        $queryUP = "UPDATE `109173_project`.`profilepic` SET `flag` = '" . $this->id . "' WHERE 1";
//        mysql_query($queryUP);
        return mysql_fetch_assoc($result);
    }

    public function trash() {
        $query = "UPDATE `109173_project`.`profilepic` SET `deleted_at` = '" . date("Y-m-d h:i:s") . "' WHERE `profilepic`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'index.php';</script>";
        } else {
            echo 'somthing Went Wrong';
        }
    }

    public function trashedItems() {
        $allData = array();
        $query = "SELECT * FROM `profilepic` WHERE `deleted_at` != 0";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $allData [] = $row;
        }

        return $allData;
    }

    public function delete() {
        $query = "DELETE FROM `109173_project`.`profilepic` WHERE `profilepic`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'trashedItems.php';</script>";
        } else {
            echo 'Something went wrong';
        }
    }

    public function restore() {
        $query = "UPDATE `109173_project`.`profilepic` SET `deleted_at` = '" . 0 . "' WHERE `profilepic`.`id` =" . $this->id;
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'index.php';</script>";
        } else {
            echo 'somthing Went Wrong';
        }
    }

    public function update() {
        if (empty($this->image)) {
            $query = "UPDATE `profilepic` SET `username`='" . $this->name . "',`updated`='" . date("Y-m-d h:i:s") . "' WHERE `id` =" . $this->id;
        } else {
            $query = "UPDATE `profilepic` SET `username`='" . $this->name . "',`imagename`='" . $this->image . "',`updated`='" . date("Y-m-d h:i:s") . "' WHERE `id` =" . $this->id;
        }
        if (mysql_query($query)) {
            ob_clean();
            ob_flush();
            @header('Location: index.php');
            echo "<script>window.location = 'index.php';</script>";
        } else {
            echo 'somthing Went Wrong';
        }
    }

    public function proPic() {
        $data = array();
//        $flag  = mysql_query("SELECT distinct `flag` FROM `profilepic` WHERE 1");
//        echo $flag;
//        die();
        $query = "SELECT * FROM `profilepic` ORDER BY `profilepic`.`flag` ASC ";
        $result = mysql_query($query);
        while ($row = mysql_fetch_assoc($result)) {
            $data = $row;
        }
        return $data;
    }

}
