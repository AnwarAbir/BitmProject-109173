<?php
$conn = mysql_connect('localhost', 'root', '') or die('can nit connect to DB');
mysql_select_db('brighter2morrow', $conn) or die('can not connect to db');

function build_category_tree(&$output, $preselected, $parent = 0, $indent = "") {
    $r = mysql_query("SELECT `id`,`title` FROM `categories` WHERE `parent_id` =" . $parent . "");

    while ($c = mysql_fetch_array($r, MYSQL_ASSOC)) {
        $selected = ($c["id"] == $preselected) ? "selected=\"selected\"" : "";
        $output .= "<option value=\"" . $c["id"] . "\" " . $selected . ">" . $indent . $c["title"] . "</option>";
        if ($c["id"] != $parent) {
            build_category_tree($output, $preselected, $c["id"], $indent . "&nbsp;&nbsp;");
        }
    }
}

build_category_tree($categories, 0);
// if you want to preselect a value and start from some subcategory
build_category_tree($categories, 5, 2);
?>

<!-- HTML -->
<select><?php echo $categories ?></select>
<!-- if you want to add some extra options -->
<select>
    <option value="-1">Choose a category</option> 
<?php echo $categories ?>
</select>