<?php
/* Getting the data to show in drop down list */
$dbUser = 'root';
$dbPass = '';
try {
    $conn = new PDO('mysql:host=localhost;dbname=brighter2morrow', $dbUser, $dbPass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}
$sql = $conn->prepare("SELECT * FROM `categories`");
$sql->execute();
$results = $sql->fetchAll();
echo "<pre>";
print_r($results);
echo "<pre>";
?>
<html>
    <form method="post">
        <select name="class">
            <option value="">SELECT CLASS</option>
            <?php
            foreach ($results as $value) {
                echo '<option value="' . $value["id"] . '">' . $value["title"] . '</option>';
            }
            ?>
        </select>
    </form>