<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Array</title>
</head>
<body>

</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 2/16/2016
 * Time: 9:57 AM
 */

//Indexed Array
echo "Indexed Array";
$Arr = array('My ', 'Name', 'is ', 'Abir');

echo "<pre>";
print_r($Arr);
echo "</pre>";

echo "<pre>";
var_dump($Arr);
echo "</pre>";

echo $Arr[3] . "</Br>";
// End Indexed Array

//Associative Array
echo "Indexed Array";
$Arr1 = array('name' => 'Abir', 'Address' => 'Dhaka', 'Jagannath University');
echo "<pre>";
print_r($Arr1);
echo "</pre>";

echo "<pre>";
var_dump($Arr);
echo "</pre>";
