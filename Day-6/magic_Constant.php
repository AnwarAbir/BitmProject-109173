<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Default Constant</title>
    </head>
    <body>
        <?php

        // put your code here
        function add($num1, $num2) {
            $result = $num1 + $num2;
            return $result;
        }

        echo add(5, 6);

        echo "<br/>Line num is " . __LINE__;
        echo "<Br/>File path " . __FILE__;
        ?>
    </body>
</html>
