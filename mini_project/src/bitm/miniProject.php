<?php

namespace App\bitm;

use PDO;

class miniProject {

    public $id = '';
    public $userName = '';
    public $email = '';
    public $password = '';
    public $dbUser = 'root';
    public $dbPass = '';
    public $conn;
    public $firstName = '';
    public $lastName = '';
    public $personalPhone = '';
    public $homePhone = '';
    public $officePhone = '';
    public $currentAddress = '';
    public $permanentAddress = '';
    public $image = '';
    public $userAuth = '';

    public function __construct() {
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=usrreg', $this->dbUser, $this->dbPass);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function setValues($array = array()) {
        $this->email = $array['email'];
        $this->password = $array['password'];
        return $this;
    }

    function setId($id) {
        return $this->id = $id;
    }

    function setUserName($userName) {
        $this->userName = $userName;
    }

    function setEmail($email) {
        return $this->email = $email;
    }

    function setValuesProfile($data = array()) {
        $this->firstName = $data['fname'];
        $this->lastName = $data['lname'];
        $this->personalPhone = $data['pno'];
        $this->homePhone = $data['hno'];
        $this->officePhone = $data['ono'];
        $this->currentAddress = $data['cAddress'];
        $this->permanentAddress = $data['pAddress'];
        $this->email = $data['email'];
        if (isset($data['image'])) {
            $this->image = $data['image'];
        }
    }

    public function logIn() {
        $statement = $this->conn->prepare("SELECT * FROM `users` WHERE `email` = :email");
        try {
            $statement->execute(array(':email' => "$this->email"));
            return $statement->fetch();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function register() {
        $sql = "INSERT INTO `usrreg`.`users` (`id`, `unique_id`, `username`, `password`, `email`,  `created_at`, `is_active`) VALUES (NULL, '" . uniqid() . "', :userName, :password, :email, '" . date("Y-m-d h:i:s") . "',  NULL)";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(
                    array(
                        ':userName' => "$this->userName",
                        ':password' => "$this->password",
                        ':email' => "$this->email"
                    )
            );
            header('location:index.php');
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function index() {
        $statement = $this->conn->prepare("SELECT * FROM `users` WHERE `email` != 'sumon@gmail.com' ");
        try {
            $statement->execute();
            return $statement->fetchAll();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function stroe() {
        $query = "SELECT `id` FROM `users` WHERE `email`='" . $this->email . "'";
        $stm1 = $this->conn->prepare($query);
        $stm1->execute();
        $this->id = $stm1->fetch()['id'];
        $sql = "INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `personal_phone`, `home_phone`, `office_phone`, `current_address`, `permanent_address`, `profile_pic`, `created_at`) VALUES (NULL, :id, :fname, :lname, :pno, :hno, :ono, :cAddress, :pAddress, :image, '" . date("Y-m-d h:i:s") . "')";
        try {
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(
                    array(
                        ':id' => "$this->id",
                        ':fname' => "$this->firstName",
                        ':lname' => "$this->lastName",
                        ':pno' => "$this->personalPhone",
                        ':hno' => "$this->homePhone",
                        ':ono' => "$this->officePhone",
                        ':cAddress' => "$this->currentAddress",
                        ':pAddress' => "$this->permanentAddress",
                        ':image' => "$this->image"
                    )
            );
            header('location:dashboard.php');
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function dashboard() {
        $query = "SELECT * FROM `profiles` WHERE `user_id` in (SELECT `id` FROM `users` WHERE `email` = :email)";
        $statement = $this->conn->prepare($query);
        try {
            $statement->execute(array(':email' => "$this->email"));
            return $statement->fetch();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function dashboard_user() {
        $statement = $this->conn->prepare("SELECT * FROM `users` WHERE `email` = :email");
        try {
            $statement->execute(array(':email' => "$this->email"));
            return $statement->fetch();
        } catch (PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }
    }

    public function activeUser($auth) {
        $this->userAuth = $auth;
        if ($this->userAuth == "1") {
            $query = "UPDATE `usrreg`.`users` SET `is_active` = NULL WHERE `users`.`id` = " . $this->id;
        } else {
            $query = "UPDATE `usrreg`.`users` SET `is_active` = '1' WHERE `users`.`id` = " . $this->id;
        }
        $statement = $this->conn->prepare($query);
        $statement->execute();
        ob_clean();
        ob_flush();
        @header('Location: index.php');
        echo "<script>window.location = 'index.php';</script>";
//        return $statement->fetch();
    }

    public function update() {
        if (!empty($this->image)) {
            $query = "UPDATE `profiles` SET `first_name`=:fname,`last_name`=:lname,`personal_phone`=:pno,`home_phone`=:hno,`office_phone`=:ono,`current_address`=:cAddress,`permanent_address`=:pAddress,`profile_pic`='" . $this->image . "',`modified_at`='" . date("Y-m-d h:i:s") . "' WHERE `id` = :id";
        } else {
            $query = "UPDATE `profiles` SET `first_name`=:fname,`last_name`=:lname,`personal_phone`=:pno,`home_phone`=:hno,`office_phone`=:ono,`current_address`=:cAddress,`permanent_address`=:pAddress,`modified_at`='" . date("Y-m-d h:i:s") . "' WHERE `id` = :id";
        }
        $statement = $this->conn->prepare($query);
        $statement->execute(
                array(
                    ':id' => "$this->id",
                    ':fname' => "$this->firstName",
                    ':lname' => "$this->lastName",
                    ':pno' => "$this->personalPhone",
                    ':hno' => "$this->homePhone",
                    ':ono' => "$this->officePhone",
                    ':cAddress' => "$this->currentAddress",
                    ':pAddress' => "$this->permanentAddress",
                )
        );
        header("location:dashboard.php");
    }

}
