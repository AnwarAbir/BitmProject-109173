<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Case</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="../../js/bootstrap.js" type="text/javascript"></script>
        <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../js/npm.js" type="text/javascript"></script>
    </head>
    <body>

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="../bitm/index.php">Brighter2morrow</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active"><a href="../bitm/index.php">Home</a></li>
                    <li><a href="../bitm/profile.php">Profile</a></li>
                    <li><a href="../bitm/dashboard.php">Dashboard</a></li>
                    <li><a href="../../logOut.php">Log Out</a></li>
                </ul>
            </div>
        </nav>
    </body>
</html>