<?php
include_once '../../vendor/autoload.php';

use App\bitm\miniProject;
use App\utility\utility;

session_start();
if (isset($_SESSION['email']) && !empty($_SESSION['email'])) {
//    $message = $_SESSION['email'];
//    echo "<script type='text/javascript'>alert('Succesfully');</script>";
    ?>   

    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Mini Project</title>
            <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
            <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <script src="../../js/bootstrap.js" type="text/javascript"></script>
            <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
            <script src="../../js/npm.js" type="text/javascript"></script>
        </head>
        <body>
            <?php
            include_once '../menu/menu.php';

            $dashboard = new miniProject();
            $dashboard->setEmail($_SESSION['email']);
            $allData = $dashboard->dashboard();

            $userData = $dashboard->dashboard_user();
            $debug = new utility();
//            $debug->debug($allData);
//            $debug->debug($_SESSION['email']);
//            die();
            ?>
            <div class="container">
                <h3 class="text-center">Profile Information</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-lg-offset-3 text-center">
                                <img src="../../image/<?php echo $allData['profile_pic']; ?>" width="200" height="200">                            
                            </div>
                        </div>
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>User Name</td>
                                    <td><?php echo $allData['first_name']; ?></td>
                                </tr>
                                <tr>
                                    <td>Last Name</td>
                                    <td><?php echo $allData['last_name']; ?></td>
                                </tr>
                                <tr>
                                    <td>Personal Phone Number</td>
                                    <td><?php echo $allData['personal_phone']; ?></td>
                                </tr>
                                <tr>
                                    <td>Home Number</td>
                                    <td><?php echo $allData['home_phone']; ?></td>
                                </tr>
                                <tr>
                                    <td>Office Number</td>
                                    <td><?php echo $allData['office_phone']; ?></td>
                                </tr>
                                <tr>
                                    <td>Current Address</td>
                                    <td><?php echo $allData['current_address']; ?></td>
                                </tr>
                                <tr>
                                    <td>Permanent Address</td>
                                    <td><?php echo $allData['permanent_address']; ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a class="btn btn-success" href="edit.php?update=profile">Update</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h3 class="text-center">User Information</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <td>User Name</td>
                                    <td><?php echo $userData['username']; ?></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><?php echo $userData['email']; ?></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a class="btn btn-success" href="edit.php?update=user">Update</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </body>
    </html>
    <?php
} else {
    header("location:../../index.php");
}
?>