<?php
include_once '../../vendor/autoload.php';

use App\bitm\miniProject;
use App\utility\utility;

session_start();
if (isset($_SESSION['email']) && !empty($_SESSION['email'])) {
//    $message = $_SESSION['email'];
//    echo "<script type='text/javascript'>alert('Succesfully');</script>";
    ?>   

    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Mini Project</title>
            <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
            <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <script src="../../js/bootstrap.js" type="text/javascript"></script>
            <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
            <script src="../../js/npm.js" type="text/javascript"></script>
        </head>
        <body>
            <?php
            include_once '../menu/menu.php';
            ?>
            <div class="container">
                <h3 class="text-center">Profile Information</h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form role="form" action="store.php" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="fname">First Name:</label>
                                <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name"/><br/>
                            </div>
                            <div class="form-group">
                                <label for="lname">Last Name:</label>
                                <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name"/><br/>
                            </div>
                            <div class="form-group">
                                <label for="pno">Personal Phone Number:</label>
                                <input type="text" class="form-control" name="pno" id="pno" placeholder="01 xxx xxx xxx"/><br/>
                            </div>
                            <div class="form-group">
                                <label for="hno">Home Number:</label>
                                <input type="text" class="form-control" name="hno" id="hno" placeholder="01 xxx xxx xxx"/><br/>
                            </div>
                            <div class="form-group">
                                <label for="ono">Office Number:</label>
                                <input type="text" class="form-control" name="ono" id="ono" placeholder="xxx xxx xxx"/><br/>
                            </div>
                            <div class="form-group">
                                <label for="cAddress">Current Address:</label>
                                <input type="text" class="form-control" name="cAddress" id="cAddress" placeholder="Street/Village City District"/><br/>
                            </div>
                            <div class="form-group">
                                <label for="pAddress">Permanent Address:</label>
                                <input type="text" class="form-control" name="pAddress" id="pAddress" placeholder="Street/Village City District"/><br/>
                            </div>
                            <div class="form-group">
                                <label for="image">Upload Profile Picture</label>
                                <input type="file" name="image" id="image"/><br/>
                            </div>
                            <input type="hidden" name="email" value="<?php echo $_SESSION['email']; ?>"/>
                            <div class="form-group">
                                <input type="submit" name="submit" value="Save"/>
                                <input type="reset" value="Reset"/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </body>
    </html>
<?php
} else {
    header("location:../../index.php");
}
?>