<?php
include_once '../../vendor/autoload.php';

use App\bitm\miniProject;
use App\utility\utility;
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if (isset($_FILES['image'])) {
            $image_name = time() . "_" . $_FILES['image']['name'];
            $image_type = $_FILES['image']['type'];
            $image_location = $_FILES['image']['tmp_name'];
            $image_error = $_FILES['image']['error'];
            $image_size = $_FILES['image']['size'];

            $img_name_array = explode(".", $image_name);
            $image_extension = strtolower(end($img_name_array));

            $formate = array('png', 'jpg', 'jpeg');

            if (in_array("$image_extension", $formate)) {
                $_POST['image'] = $image_name;
                move_uploaded_file($image_location, '../../image/' . $image_name);
            } else {
                echo 'wrong file';
            }
        }
//        print_r($_POST);
        $update = new miniProject();
        $update->setId($_POST['id']);
        $update->setValuesProfile($_POST);
        $update->update();
        ?>
    </body>
</html>
