<?php
include_once '../../vendor/autoload.php';

use App\bitm\miniProject;
use App\utility\utility;

session_start();
if (isset($_SESSION['email']) && !empty($_SESSION['email'])) {
//    $message = $_SESSION['email'];
//    echo "<script type='text/javascript'>alert('Succesfully');</script>";
    ?>   

    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="UTF-8">
            <title>Mini Project</title>
            <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
            <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <script src="../../js/bootstrap.js" type="text/javascript"></script>
            <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
            <script src="../../js/npm.js" type="text/javascript"></script>
        </head>
        <body>
            <?php
            include_once '../menu/menu.php';

            $edit = new miniProject();

            if ($_GET['update'] == 'profile') {
                $edit->setEmail($_SESSION['email']);
                $data = $edit->dashboard();

//            $debug = new utility();
//            $debug->debug($_GET);
//            die();
                ?>
                <div class="container">
                    <h3 class="text-center">Update Profile Information</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form role="form" action="update.php" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="fname">First Name:</label>
                                    <input type="text" class="form-control" name="fname" id="fname" placeholder="First Name" value="<?php echo $data['first_name']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="lname">Last Name:</label>
                                    <input type="text" class="form-control" name="lname" id="lname" placeholder="Last Name" value="<?php echo $data['last_name']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="pno">Personal Phone Number:</label>
                                    <input type="text" class="form-control" name="pno" id="pno" placeholder="01 xxx xxx xxx"  value="<?php echo $data['personal_phone']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="hno">Home Number:</label>
                                    <input type="text" class="form-control" name="hno" id="hno" placeholder="01 xxx xxx xxx"  value="<?php echo $data['home_phone']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="ono">Office Number:</label>
                                    <input type="text" class="form-control" name="ono" id="ono" placeholder="xxx xxx xxx"  value="<?php echo $data['office_phone']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="cAddress">Current Address:</label>
                                    <input type="text" class="form-control" name="cAddress" id="cAddress" placeholder="Street/Village City District"  value="<?php echo $data['current_address']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="pAddress">Permanent Address:</label>
                                    <input type="text" class="form-control" name="pAddress" id="pAddress" placeholder="Street/Village City District"  value="<?php echo $data['permanent_address']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="image">Upload Profile Picture</label>
                                    <input type="file" name="image" id="image"/><br/><img src="../../image/<?php echo $data['profile_pic']; ?>" width="200" height="200">
                                </div>
                                <input type="hidden" name="email" value="<?php echo $_SESSION['email']; ?>"/>
                                <input type="hidden" name="id" value="<?php echo $data['id']; ?>"/>
                                <div class="form-group">
                                    <input type="submit" name="submit" value="Update"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                $editUserInfo = new miniProject();
                $editUserInfo->setEmail($_SESSION['email']);
                $dataUser = $editUserInfo->logIn();
//                $debug = new utility();
//                $debug->debug($dataUser);
                ?>
                <div class="container">
                    <h3 class="text-center">Update User Information</h3>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form role="form" action="update.php" method="post">
                                <div class="form-group">
                                    <label for="username">User Name:</label>
                                    <input type="text" class="form-control" name="username" id="lname" value="<?php echo $dataUser['username']; ?>" disabled/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email:</label>
                                    <input type="text" class="form-control" name="email" id="fname" value="<?php echo $dataUser['email']; ?>"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="oldpwd">Old Password:</label>
                                    <input type="password" class="form-control" id="oldpwd"/><br/>
                                </div>
                                <div class="form-group">
                                    <label for="newpwd">New Password:</label>
                                    <input type="password" class="form-control" name="newpwd" id="newpwd"/><br/>
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="submit" value="Update"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <?php }
            ?>
        </body>
    </html>
<?php }
?>