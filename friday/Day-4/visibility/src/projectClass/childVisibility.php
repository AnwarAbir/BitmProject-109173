<?php

namespace App\projectClass;

include_once 'vendor/autoload.php';

//include_once './visibility.php';
use App\projectClass\visibility;

class childVisibility extends visibility {

    //put your code here
    public function printConnection() {
        echo 'MY Connection';
    }

    public function getName() {
        return parent::getName();
    }
    
    public function getWork() {
        return parent::getWork();
    }
    
//    public function getWife() {
//        return parent::getWife();
//    }
}
