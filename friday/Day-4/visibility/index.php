<!DOCTYPE html>
<?php include_once './vendor/autoload.php'; ?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php

        use App\projectClass\visibility;

echo ".............From Main Class.............<Br/><Br/>";
        $visibilityOj = new visibility("A", "B", "C");
        echo $visibilityOj->getName() . "<Br/>";
        echo $visibilityOj->name . "<Br/>";

//        echo $visibilityOj->getWork(); //showing an error cause of protected property 


        use App\projectClass\childVisibility;

echo ".............From Child Class.............<Br/><Br/>";
//        $childVisibilityOj = new childVisibility();
        $childVisibilityOj = new childVisibility("S", "D", "F");
//        $childVisibilityOj->printConnection() . "<Br/>" . "<Br/>";
        echo $childVisibilityOj->name . "<Br/>";
        echo $childVisibilityOj->getName() . "<Br/>";
        echo $childVisibilityOj->getWork();
//        echo $childVisibilityOj->getWife();
        ?>
    </body>
</html>
