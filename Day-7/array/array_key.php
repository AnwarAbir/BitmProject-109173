<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Array key</title>
    </head>
    <body>
        <?php
        $arr = array("My", "Name", "is", "Abir");
        $data = array_keys($arr);
        echo "$data";
        echo "<pre>";
        print_r($arr);
        echo "<pre>";
        
        $pad = array_pad($arr, 7, "Prince");
        print_r($pad);
//
//        if (array_key_exists("0", $arr)) {
//            echo "Exist";
//        }
//        
        $flip = array_flip($arr);
        print_r($flip);
        
        $mer = array_merge($arr, $flip);
        print_r($mer);
//        ?>
    </body>
</html>
